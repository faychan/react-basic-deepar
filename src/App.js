import React, { Component } from "react";
import "./lib/deepar.js";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCapturing: true,
      isUploading: false,
      imageURL: null,
      isLoading: true,
    };
    this.myCanvasRef = React.createRef();
  }
  componentDidMount() {
    var canvasHeight = window.innerHeight;
    var canvasWidth = window.innerWidth;
    var deepAR = DeepAR({ 
      canvasWidth: canvasWidth, 
      canvasHeight: canvasHeight,
      licenseKey: 'e1dfa5092bc46693c7eb44bc38e308e553533fa603dee3026eb546a287cf872936dc17330134f772',
      canvas: this.myCanvasRef.current,
      // this.myCanvasRef.current,
      numberOfFaces: 1,
      libPath: './lib',
      segmentationInfoZip: 'segmentation.zip',
      onInitialize: function() {

        // start video immediately after the initalization, mirror = true
        deepAR.startVideo(true);

        // or we can setup the video element externally and call deepAR.setVideoElement (see startExternalVideo function below)

        deepAR.switchEffect(0, 'plot', './glasses', function() {
          // console.log("?awo")
        });
      }
    });
    // deepAR.onVideoStarted = () => {
    //   this.setState({ isLoading : false });
    deepAR.downloadFaceTrackingModel('../assets/lib/models-68-extreme.bin');
  };

  render(){
    const { isLoading } = this.state;
    return (
      <div className="App">
        <canvas className="deepar" ref={this.myCanvasRef} id="deepar-canvas" onContextMenu={event => event.preventDefault()}></canvas>
        {/* <div className={isLoading ? "hidden" : ""} id="loader-wrapper">
          <span className="loader"></span>
        </div> */}
      </div>
    );
  }
}

export default App;
